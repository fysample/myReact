/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {Provider} from 'react-redux'
import * as firebase from 'firebase';
import ItemList from './App/config/DrawerNav'
import mystore from './App/store/store'


export default class App extends Component {


    render() {
      // Initialize Firebase
      const firebaseConfig = {
        apiKey: "AIzaSyBaezGwmoo14W7ZjMUffmOl_0LLDahEKT8",
        authDomain: "myreact-fc44d.firebaseapp.com",
        databaseURL: "https://myreact-fc44d.firebaseio.com",
        storageBucket: "myreact-fc44d.appspot.com"
      };
      const firebaseApp = firebase.initializeApp(firebaseConfig);
      return (
        <Provider store={mystore}>
          <ItemList />
        </Provider>
      );
    }
}
