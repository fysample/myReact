import {StyleSheet, Platform} from 'react-native'

export default InputStyle = StyleSheet.create({
  container: {
  flex: 1,
  justifyContent: 'center',
  backgroundColor: '#ffffff',
  padding: 50

},
  border: {
  position: 'relative',
  borderColor: '#b0bac5',
  marginTop: 12
  }
});
