import {StyleSheet} from 'react-native'
export default ConfirmModalStyle = StyleSheet.create({
  title: {
    fontSize: 17,
    paddingLeft: 13,
    paddingRight: 13,
    fontWeight: "600",
    color: '#252525',
    paddingTop: 17,
    paddingBottom: 13,
    textAlign: 'center'
  },
  description: {
    fontSize: 13,
    paddingLeft: 13,
    paddingRight: 13,
    color: '#252525',
    textAlign: 'center',
    paddingBottom: 28
  },
  confirm: {
    borderColor: '#ebebeb',
    borderTopWidth: 1,
    paddingTop: 10,
    paddingBottom: 5,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  add: {
    fontSize: 17,
    color: '#1f8be6',
    paddingLeft: 20,
    paddingRight: 20
  },
  cancel: {
    fontSize: 17,
    color: '#1f8be6',
    paddingLeft: 20,
    paddingRight: 20
  }
});
