import { FETCHING_DATA, FETCHING_DATA_SUCCESS, FETCHING_DATA_FAILURE, UPDATE_DATA_LOGIN, UPDATE_DATA_LOGIN_FETCH } from '../constants'
import getPeople from '../mock_api/api'


export function getData() {
  return {
    type: FETCHING_DATA,

  }
}

export function getDataSuccess(data) {

  return {
    type: FETCHING_DATA_SUCCESS,
    data
  }
}

export function getDataFailure() {
  return {
    type: FETCHING_DATA_FAILURE
  }
}

export function getDataLogin(payload:any) {
  return {
    type: UPDATE_DATA_LOGIN_FETCH,
    payload: payload
  }
}

export function handleUpdateDataLogin(data) {
    return {
      type: UPDATE_DATA_LOGIN,
      data
    }
}



export function fetchData() {
  return (dispatch) => {
    dispatch(getData())
    getPeople()
        .then((data) => dispatch(getDataSuccess(data)))
        .catch(()=> dispatch(getDataFailure()))
  }
}
