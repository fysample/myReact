import React from 'react'
import {Text, Platform, TouchableOpacity, View} from 'react-native'
import {StackNavigator, TabNavigator} from 'react-navigation'
import ItemList from '../component/ItemList'
import User from '../component/User'
import Dash2 from '../component/Dashboard'

const DashbooardHomeNavigator = StackNavigator ({
  DashboardHome: {
    screen: ItemList
  },
  TestScreen: {
    screen: User
  },
  Dash2: {
    screen:Dash2
  }
},{initialRouteName: "DashboardHome"});

const FooterTabs = TabNavigator({
  Home: {
    screen: DashbooardHomeNavigator,
    navigationOptions: {
      tabBarLabel: 'Home'
    }
  },
  User: {
    screen: User,
    navigationOptions: {
      tabBarLabel: 'User'
    }
  }
},{
  tabBarPosition: 'bottom',
  tabBarOptions: {
    activeTintColor: 'white',
    inactiveTintColor: 'white',
    labelStyle: {
            fontSize: 9.5
        },
        style: {
            backgroundColor: '#0257af'
        }
  }
},);

export default FooterTabs;
