import React from 'react';
import { Button, Platform, ScrollView, StyleSheet } from 'react-native';
import { TabNavigator, DrawerNavigator } from 'react-navigation';
import MainDash from './routes'
import User from '../component/User'
import Dash2 from '../component/Dashboard'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
const TabsInDrawer = DrawerNavigator({
  MainDash: {
    screen: MainDash,
    navigationOptions: {
      drawer: () => ({
        label: 'Simple Tabs',
        icon: ({ tintColor }) => (
          <MaterialIcons
            name="filter-1"
            size={24}
            style={{ color: tintColor }}
          />
        ),
      }),
    },
  }
});


export default TabsInDrawer;
