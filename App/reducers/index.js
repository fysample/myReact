import {combineReducers} from 'redux'
import appDataReducer from './reducerUser'

const rootReducer = combineReducers({
  appData : appDataReducer,
})

export default rootReducer
