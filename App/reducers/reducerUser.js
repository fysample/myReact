import { FETCHING_DATA, FETCHING_DATA_SUCCESS, FETCHING_DATA_FAILURE,UPDATE_DATA_LOGIN, UPDATE_DATA_LOGIN_FETCH} from '../constants'
const initialState = {
  data: [],
  email: null,
  password: null,
  fullName: null,
  phoneNum:null,
  confirmPassword:null,
  dataFetched: false,
  isFetching: false,
  dataLogin: false,
  error: false
}

export default function dataReducer (state = initialState, action) {
  switch (action.type) {
    case FETCHING_DATA:
      return {
        ...state,
        data: [],
        isFetching: true
      }
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataFetched:true,
        data: action.data
      }
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        dataFetched:false,
        error: true
      }
    case UPDATE_DATA_LOGIN_FETCH:
      return {
        ...state,
        dataLogin: true,
        email:action.payload.username,
        password:action.payload.password
      }
      break;
      case UPDATE_DATA_LOGIN:
        switch (action.data.type) {
          case "email":
          return {
            ...state,
            email: action.data.value
          }
            break;
            case "password":
            return {
              ...state,
              password: action.data.value
            }
              break;
          default:

        }
    default:
      return state
  }
}
