import React, {Component} from 'react'
import Modal from 'react-native-simple-modal';
import {Text, ScrollView, View, TouchableOpacity} from 'react-native';
import ConfirmModalStyle from '../styles/modalPopup';

export  default class ModalPopup extends Component {
  constructor(props : any) {
        super(props);
    }


  render(){

    const {isOpenModal, title, handleModal, description} = this.props;

    return(
      <Modal open={isOpenModal} closeOnTouchOutside={false} style={{ alignItems: 'center', zIndex: 25}}>
        <View style={ConfirmModalStyle.container}>
          <ScrollView>
            <Text style={ConfirmModalStyle.title}>{title}</Text>
            <Text style={ConfirmModalStyle.description}>{description}</Text>
              <View style={ConfirmModalStyle.confirm}>
                <TouchableOpacity onPress={handleModal}>
                  <Text style={ConfirmModalStyle.cancel}>Cancel</Text>
                </TouchableOpacity>
              </View>
          </ScrollView>
        </View>
      </Modal>

    )
  }
}
