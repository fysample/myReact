import React, {Component}  from 'react';
import PropTypes from 'prop-types';
import {View, TextInput, Text} from 'react-native';
import InputStyle from '../styles/inputCompStyle'

export default class InputComponent extends Component {
  constructor (props : any) {
    super(props);

    this.state = {
      isFocus: false,
      text: ''
    }
  }

    handleChange = (text) => {
      this.setState({
        text:text
      });
      this.props.handleChangeText({type: this.props.name, value: text});
    }


  render() {
    const {isFocus, text} = this.state;
    const {placeholder, name, value, editable, error} = this.props;

    const secureTextEntry = (name === 'password') ? true:false;

    return(
      <View style={InputStyle.border}>
        <TextInput name={name} placeholder={placeholder} value={value ? value.toString() : text} onChangeText={(text) => this.handleChange(text)} secureTextEntry={secureTextEntry}/>
      </View>
    );
  }


}
