import React, {Component} from 'react'
import { TouchableHighlight, View, Text, Button, StyleSheet,TouchableOpacity } from 'react-native'
import ConfirmModal from './ModalPopup'
import { connect } from 'react-redux'
import { fetchData } from '../actions/actionUser'


class myReact extends Component {
  constructor(){
    super();
    this.state = {
      email: '',
      password: '',
      isShowModal: false
    }
  }

  handleModal= () => {
    this.setState({
      isShowModal : !this.state.isShowModal
    });
  }

  OpenModal = () => {
    this.handleModal()
    }


  render() {
    console.log(this.props);

    const {isShowModal} = this.state;
    const {email} = this.props;
    return (

      <View style={styles.container}>
        <Text>{email}</Text>
        <TouchableOpacity onPress={() => this.OpenModal()}>
        <Text>Open modal</Text>
      </TouchableOpacity>

          <ConfirmModal isOpenModal={isShowModal} title="Test Modal" description="My modal description" handleModal={this.handleModal}/>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  email: state.appData.email
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  textStyles: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
})

export default connect(
  mapStateToProps
)(myReact);
