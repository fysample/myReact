import React, {Component} from 'react'
import { TouchableHighlight, View, Text, Button, TextInput, StyleSheet,AsyncStorage } from 'react-native'
import InputComponent from './inputComponent'
import { connect } from 'react-redux'
import { fetchData, handleUpdateDataLogin, getDataLogin } from '../actions/actionUser'
import ConfirmModal from './ModalPopup'
import InputStyle from '../styles/inputCompStyle'

class myReact extends Component {
  constructor(){
    super();
    this.state = {
      email: '',
      password: '',
      isShowModal: false
    }
  }

  handleModal= () => {
    this.setState({
      isShowModal : !this.state.isShowModal
    })
  }

  OpenModal = () => {
    this.setState({
      isShowModal: true
    })
    }



/*  handleEmail = (text) => {
    this.setState({ email: text})
  }

  handlePassword = (text) => {
    this.setState({ password: text})
  }

  login = (email, pass) => {
    alert('email: ' + email + ' password: ' + pass)
  }*/
  fetchData = () => {
    this.props.dispatch(fetchData());
  }

  handleChangeData = (data) => {
    this.props.dispatch(handleUpdateDataLogin({type: data.type, value: data.value}));
  }

  login = (appData:any) => {
    this.props.dispatch(getDataLogin(this.props.appData));

    console.log(this.props.email);
  }

  render() {
    const {isShowModal} = this.state;
    const {email, password} = this.props;

    console.log(this.props);
    return (
      <View style={InputStyle.container}>


      <InputComponent name="email" placeholder="Email" handleChangeText = {this.handleChangeData} value={email}/>
      <InputComponent name="password" placeholder="Password" handleChangeText ={this.handleChangeData} value={password}/>
      <Button onPress={() => this.login(this)} title="Login"></Button>
      <Button onPress={() => this.props.navigation.navigate('Dash2')} title="next"></Button>


       <Button onPress={() => this.fetchData()} title="Click Me"></Button>
        {
        this.props.appData.isFetching && <Text>Loading</Text>
      }
      {
        this.props.appData.data.length ? (
          this.props.appData.data.map((person, i) => {
            return <View key={i} >
            <Text style={styles.textStyle}>Name: {person.name}</Text>
            <Text style={styles.textStyle}>Age: {person.age}</Text>

            </View>
          })
        ) : null
      }


      </View>
    );
  }
}

const mapStateToProps = state => ({
  appData: state.appData,
  email: state.appData.email,
  password: state.appData.password
})

/*const mapDispatchToProps = dispatch => ({
  fetchData: () => dispatch(fetchData()),
  handleChangeData : (data) => {
    dispatch(handleUpdateDataLogin({type: data.type, value: data.value}));
  }
})*/

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'column',
    backgroundColor: '#F5FCFF',
  },
  textStyles: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
})

export default connect(
  mapStateToProps
)(myReact);
