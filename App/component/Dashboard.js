import React, {Component} from 'react'
import { TouchableHighlight, View, Text, Button, StyleSheet } from 'react-native'

import { connect } from 'react-redux'
import { fetchData } from '../actions/actionUser'


class Dashboard extends Component {
  constructor(){
    super();

  }


  render() {
    console.log(this.props);
    const {email} = this.props;
    return (

      <View style={styles.container}>
        <Text>{email}</Text>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  email: state.appData.email
})


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  textStyles: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
})

export default connect(
  mapStateToProps
)(Dashboard);
