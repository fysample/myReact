const people = [
  { name: 'Fify', age: 20 },
  { name: 'Alia', age: 24 },
  { name: 'Vicky', age: 18 }
]

export default () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      return resolve(people)
    }, 2000)
  })
}
